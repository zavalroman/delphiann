unit Points;

interface

uses
  AnnTypes;

type
  TPointArray = class
  private
    num : Integer;
    point: TPointPtr;
  public
    constructor Create; overload;
    procedure AddPoint(p: TPointType; x: Integer; y: Integer);
    procedure ClearArray;
    function Size: Integer;
    procedure GetRandomPoint(var x: Integer; var y: Integer; var color: TPointType);
    function GetPoint(n: Integer): TPointPtr;
  end;

implementation

constructor TPointArray.Create;
begin
  inherited;
  point := nil;
  num := 0;
end;

procedure TPointArray.AddPoint(p: TPointType; x: Integer; y: Integer);
var
  newPoint : TPointPtr;
begin
  New(newPoint);
  newPoint.next := point;
  point := newPoint;
  point.color := p;
  point.x := x;
  point.y := y;
  num := num + 1;
end;

procedure TPointArray.ClearArray;
var
  buf : TPointPtr;
begin
  while point<>nil do
  begin
    buf := point.next;
    Dispose(point);
    point := buf;
  end;
end;

function TPointArray.Size;
begin
  Result := num;
end;

procedure TPointArray.GetRandomPoint(var x: Integer; var y: Integer; var color: TPointType);
var
  i, n: Integer;
  cursor : TPointPtr;
begin
  i := 0;
  randomize;
  n := Random(num);
  cursor := GetPoint(n);
  x := cursor.x;
  y := cursor.y;
  color := cursor.color;
end;

function TPointArray.GetPoint(n: Integer): TPointPtr;
var
  i : Integer;
  cursor : TPointPtr;
begin
  i := 0;
  cursor := point;
  while i < n do
  begin
    cursor := cursor.next;
    i := i + 1;
  end;
  Result := cursor;
end;

end.

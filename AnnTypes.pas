unit AnnTypes;

interface

type
  TRealArr = array of Real;

  TRef<AT, RT> = reference to function(X: AT): RT;
  TLayersSizes = array of Integer;
  TNeurons = TRealArr;
  TBiases  = TRealArr;
  TWeights = array of array of Real;
  TDeltas  = array of array of Real;

  TPointType = (Green, Blue);
  TPointPtr = ^TPoint;
  TPoint = record
    next : TPointPtr;
    color : TPointType;
    x : Integer;
    y : Integer;
  end;

implementation

end.

unit Layer;

interface

uses
  AnnTypes;

type
  TLayer = class
  public
    size : Integer;
    weightSize : Integer;
    neurons : TNeurons;
    biases  : TBiases;
    weights : TWeights;
    constructor Create(size: Integer; nextSize: Integer);
  end;

implementation

  { TLayer }

constructor TLayer.Create(size: Integer; nextSize: Integer);
var
  i: Integer;
begin
  inherited Create;
  self.size := size;
  weightSize := size * nextSize;
  SetLength(neurons, size);
  SetLength(biases, size);
  SetLength(weights, size);
  for i := 0 to size-1 do
    SetLength(weights[i], nextSize);
end;
end.

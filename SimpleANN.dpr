program SimpleANN;

uses
  Vcl.Forms,
  Main in 'Main.pas' {Form1},
  Layer in 'Layer.pas',
  NeuralNetwork in 'NeuralNetwork.pas',
  AnnTypes in 'AnnTypes.pas',
  Points in 'Points.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.

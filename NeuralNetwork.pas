unit NeuralNetwork;

interface

uses
  Layer, AnnTypes;

type
  TNeuralNetwork = class
  private
    learningRate: Real;
    activation: TRef<Real, Real>;
    derivative: TRef<Real, Real>;
    layers : array of TLayer;
  public
  constructor Create(learningRate: Real;
                     activation:   TRef<Real, Real>;
                     derivative:   TRef<Real, Real>;
                     const sizes:  TLayersSizes);
  function FeedForward(const inputs: TRealArr): TRealArr;
  procedure BackPropagation(targets: TRealArr);
  end;

implementation
  { TNeuralNetwork }

constructor TNeuralNetwork.Create(learningRate: Real;
                                  activation:   TRef<Real, Real>;
                                  derivative:   TRef<Real, Real>;
                                  const sizes:  TLayersSizes);
var
  i, n, nextSize : Integer;
  j: Integer;
  k: Integer;
begin
  inherited Create;
  self.learningRate := learningRate;
  self.activation := activation;
  self.derivative := derivative;
  n := Length(sizes);
  SetLength(layers, n);
  for i := 0 to n-1 do
  begin
    nextSize := 0;
    if i<n-1 then nextSize := sizes[i+1];
    layers[i] := TLayer.Create(sizes[i], nextSize);
    for j := 0 to sizes[i]-1 do
    begin
      Randomize;
      layers[i].biases[j] := Random;
      for k := 0 to nextSize-1 do
        layers[i].weights[j][k] := Random;
    end;
  end;
end;

function TNeuralNetwork.FeedForward(const inputs: TRealArr): TRealArr;
var
  i, j, n: Integer;
  l, l1: ^TLayer;
  k: Integer;
begin
  layers[0].neurons := inputs;
  n := Length(layers);
  for i := 1 to n-1 do
  begin
    l  := @layers[i-1];
    l1 := @layers[i];
    for j := 0 to l1.size-1 do
    begin
      l1.neurons[j] := 0;
      for k := 0 to l.size-1 do
        l1.neurons[j] := l1.neurons[j] + l.neurons[k] * l.weights[k][j];
      l1.neurons[j] := l1.neurons[j] + l1.biases[j];
      l1.neurons[j] := activation(l1.neurons[j]);
    end;

  end;
  Result := layers[n-1].neurons;
end;

procedure TNeuralNetwork.BackPropagation(targets: TRealArr);
var
  i, j, k, n, lnum : Integer;
  errors : TRealArr;
  l, l1: ^TLayer;
  errorNext : TRealArr;
  gradients : TRealArr;
  deltas    : TDeltas;
begin
  lnum := Length(layers);
  n := Length(targets);
  SetLength(errors, n);
  for i := 0 to n-1 do
    errors[i] := targets[i] - layers[lnum-1].neurons[i];
  for k := lnum-2 to 0 do
  begin
    l  := @layers[k];
    l1 := @layers[k+1];
    SetLength(errorNext, l.size);
    SetLength(gradients, l1.size);
    for i := 0 to l1.size-1 do
    begin
      gradients[i] := errors[i] * derivative(l1.neurons[i]);
      gradients[i] := gradients[i] * learningRate;
    end;
    SetLength(deltas, l1.size, l.size);
    for i := 0 to l1.size-1 do
      for j := 0 to l.size-1 do
      begin
        deltas[i][j] := gradients[i] * l.neurons[j];
      end;
    for i := 0 to l.size-1 do
    begin
      errorNext[i] := 0;
      for j := 0 to l1.size-1 do
        errorNext[i] := errorNext[i] + l.weights[i][j] * errors[j];
    end;
    errors := errorNext;
    for i := 0 to l1.size-1 do
      for j := 0 to l.size-1 do
        l.weights[j][i] := l.weights[j][i] + deltas[i][j];
    for i := 0 to l1.size-1 do
      l1.biases[i] := l1.biases[i] + gradients[i];
  end;
end;

end.
